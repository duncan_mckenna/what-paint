# README #

### What is this repository for? ###

* This is an application to see what paints you need for whatever Citadel scheme. Currently the application starts off with a default set of paints un the users collection. These can be edited by clicking through the library at the bottom of the page. 
* version .1

### How do I get set up and run? ###

From the terminal:

```
npm install
grunt
```

### Who do I talk to? ###

* Developed by Duncan Mckenna.